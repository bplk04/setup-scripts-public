#!/bin/bash

. /etc/os-release

if [ $1 ]
then
    echo "not adding repo because of ci mode"
else
    wget https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$VERSION_ID.noarch.rpm
    rpm -Uvh rpmfusion-free-release-$VERSION_ID.noarch.rpm
fi

dnf remove meson ninja
dnf install -y python3 python3-pip clang SDL2-devel SDL2_image-devel SDL2_gfx-devel glm-devel ffmpeg-devel gcc-c++ git wget git-lfs
python3 -m pip install --user meson ninja

if [ $1 ]
then
    git clone https://git.thm.de/bahn-simulator/simulator.git
else
    git clone --recursive https://git.thm.de/bahn-simulator/simulator.git
fi

cd simulator
meson setup build
meson compile -C build
meson test -C build
cd ..

