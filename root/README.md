# Root setup files

This folder contains setup scripts that can only be executed by users with superuser/admin permissions.

## linux

To run the scripts you will have to call sudo manually eg. `sudo bash apt.sh`.

### ubuntu (focal or newer) & debian (bullseye or newer)

On debian based distribution you can run either apt.sh or root_install.ps1.
Both will install all of the dependencies, clone the code and build the simulator.

### fedora 34

You can run the dnf.sh script on fedora 34.
It will install all of the required dependencies and clone and build the simulator.

## windows

Please use the msys2 installation as described in the user scripts.

## mac osx

Mac OSX is not oficially supported yet.
If you have all of the build dependencies (libav-dev, sdl2-dev, meson, git, git-lfs & clang++-9) you can try your luck and run the powershell script.
Feel free to open an issue and report your findings.
