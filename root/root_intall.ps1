#requires -version 6.0
#requires –RunAsAdministrator

function cloneAndBuild () {
    Write-Output "cloning the source"
    git clone https://git.thm.de/bahn-simulator/simulator.git
    cd simulator

    Write-Output "downloading the data"
    git submodule init
    git submodule update

    Write-Output "starting the build of the source"
    meson setup build
    meson compile -C build
    meson test -C build


    cd ..
}

$oldPreference = $ErrorActionPreference
$ErrorActionPreference = 'stop'

#determin what operating system is used to run the srict and install the dependency in the correct way

if($IsLinux){
    Write-Output "running on Linux"

    try {
        if(Get-Command apt){
            #download the dependencies
            Write-Output "apt exists, installing dependencies, this requires sudo rights by default"
            sudo apt-get update
            sudo apt-get remove --yes meson
            sudo apt-get install --yes gcc-10 git curl python3-pip
            sudo -H python3 -m pip install meson ninja
            sudo apt-get install --yes libopencv-dev libsdl2-dev libavcodec-dev libavdevice-dev libavfilter-dev libavformat-dev libavresample-dev libavutil-dev libswresample-dev libswscale-dev libpostproc-dev
            curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
            
            #start the download and build process
            cloneAndBuild
        }
    }Catch {
        Write-Output "apt does not found. At the moment only debian based distributions are supported"
    }Finally {$ErrorActionPreference=$oldPreference}
}

if($IsWindows){
    Write-Output "running on Windows"
    Write-Output "!!Please use the msys2 installation on windows!!"
}

if($IsMacOS){
    Write-Output "running on Mac Os"
    Write-Output "!!Mac Os is not supported at the moment so you have to install the dependencies yourself!!"

    try {
        #if(Get-Command apt){
            
            #download the dependencies
            #Write-Output "apt exists, installing dependencies"
            #choco install meson llvm ninja ffmpeg git git-lfs
            
            #start the download and build process
        cloneAndBuild
        #}
    }Catch {
        Write-Output "something went wrong"
    }Finally {$ErrorActionPreference=$oldPreference}
}
