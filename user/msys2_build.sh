#!/bin/bash

git clone https://git.thm.de/bahn-simulator/simulator.git

cd simulator

git submodule init
git submodule update

cd data
git-lfs install
git-lfs pull
cd ..

meson setup -Dlibtrainsim:video_use_vsync=false -Ddefault_library=static -Db_lto=false build
meson compile -C build
meson test -C build
cd ..
