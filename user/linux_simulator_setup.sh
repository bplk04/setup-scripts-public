#!/bin/bash

ORANGE='\033[0;33m'
NO_COLOR='\033[0m'

echo ""
echo "${ORANGE} This script needs a working installation of python3 with pip, a recent compiler toolchain (gcc >9.2 or clang >10.0) and git-lfs!${NO_COLOR}"
echo ""

python3 -m pip install --user meson ninja

if [ $1 ]
then
    git clone https://git.thm.de/bahn-simulator/simulator.git
else
    git clone --recursive https://git.thm.de/bahn-simulator/simulator.git
fi
cd simulator
meson setup build
meson compile -C build
meson test -C build
cd ..
