#!/bin/bash

pacman -Sy --noconfirm pactoys-git git
pacboy --noconfirm update
pacboy sync --needed --noconfirm \
    mingw-w64-clang-x86_64-SDL2 \
    mingw-w64-clang-x86_64-SDL2_image \
    mingw-w64-clang-x86_64-SDL2_gfx \
    mingw-w64-clang-x86_64-ffmpeg \
    mingw-w64-clang-x86_64-glm \
    mingw-w64-clang-x86_64-pkg-config \
    mingw-w64-clang-x86_64-git-lfs \
    mingw-w64-clang-x86_64-meson \
    mingw-w64-clang-x86_64-cmake \
    mingw-w64-clang-x86_64-make \
    mingw-w64-clang-x86_64-toolchain \
    mingw-w64-clang-x86_64-ninja
