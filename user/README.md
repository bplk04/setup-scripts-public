# User setup files

This folder contain setup scripts that can be executed by users without superuser/admin permissions.

## linux

linux_simulator_setup.sh is a generic setup script that works on any linux distribution as long as all of the build dependencies are satisfied.
They need a working compiler toolchain (gcc >9.2 or clang >10.0) and a system installation of libav and sdl2 with development headers in order to work properly.

## windows

msys2_install_only.sh only installs the build and clone dependencies for a build using mingw64.
Because it is not possible to update running programs on windows msys2_install_only.sh might have to run more than once.
Use mingw64_build.sh to actually clone and build the simulator.
