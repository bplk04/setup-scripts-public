# setup-scripts

This repository contains setup scripts for the simulator and libtrainsim.

# Install and build

## Linux

### Ubuntu (focal and newer) or debian (bullseye ore newer)

For a quick Ubuntu setup execute the following commands:

```bash
git clone git@git.thm.de:bahn-simulator/setup-scripts.git
sudo bash ./setup-scripts/root/apt.sh
```

### Fedora

For a quick fedora setup execute the following commands:

```bash
git clone git@git.thm.de:bahn-simulator/setup-scripts.git
sudo bash ./setup-scripts/root/dnf.sh
```

## Windows 10

The windows setup works using msys2 (64bit version is required).
You can get it from [here](https://www.msys2.org/).

For a quick Windows setup type the following commands in the mingw64 terminal (you might have to restart the terminal and execute command 3 more than once):

```bash
pacman -Sy git
git clone git@git.thm.de:bahn-simulator/setup-scripts.git
bash ./setup-scripts/user/msys2_install_only.sh
bash ./setup-scripts/user/mingw64_build.sh
```

# run the simulator

In the terminal type the following command:

```bash
cd simulator
./build/simulator
```

or use the following command to run a specific Track file:

```bash
cd simulator
./build/simulator 'path/to/Track.json'
```
